using UnityEditor;
using UnityEngine;

namespace LS.Editor
{
    [CustomPropertyDrawer(typeof(ReadOnlyOnInspectorAttribute))]
    public class ReadOnlyOnInstpectorPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label);
            GUI.enabled = true;
        }
    }
}
