using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace LS.Editor
{
    [CustomPropertyDrawer(typeof(StringMaxLengthAttribute))]
    public class StringMaxLengthPropertyDrawer : PropertyDrawer
    {
        private const string handlerName = "string";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.type == handlerName && property.stringValue.Length > (this.attribute as StringMaxLengthAttribute).Value)
            {
                property.stringValue = property.stringValue.Substring(0, (this.attribute as StringMaxLengthAttribute).Value);
            }
            EditorGUI.PropertyField(position, property, label);
        }
    }
}
