using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LS
{
    [AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public class StringMaxLengthAttribute : PropertyAttribute
    {
        public readonly int Value;

        public StringMaxLengthAttribute(int value)
        {
            this.Value = value;
        }
    }
}
