using UnityEngine;
using System;

namespace LS
{
    [AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public class ReadOnlyOnInspectorAttribute : PropertyAttribute
    {
        
    }


}
